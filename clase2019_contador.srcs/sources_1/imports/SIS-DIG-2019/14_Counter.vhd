----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity Counter is
    Port ( CLK : in STD_LOGIC;
           RST : in STD_LOGIC;
           ENA : in STD_LOGIC;
           DOWN : in STD_LOGIC;
           LOAD_N : in STD_LOGIC;
           PWM : out STD_LOGIC;
           DIN : in STD_LOGIC_VECTOR (10 downto 0);
           CNT : out STD_LOGIC_VECTOR (10 downto 0));
end Counter;

architecture Behavioral of Counter is

signal CNT_INT : STD_LOGIC_VECTOR (10 downto 0);

begin

process (CLK, RST) begin
    if RST = '1' then
        CNT_INT <= (others => '0');
    elsif rising_edge(CLK) then
        if ENA = '1' then
            if LOAD_N = '0' then
                CNT_INT <= DIN;
            elsif DOWN = '1' then
                CNT_INT <= CNT_INT - 1;
            else  
                CNT_INT <= CNT_INT + 1;
            end if;
        end if;
    end if;          
end process;

CNT <= CNT_INT;

PWM1 : process(CNT_INT) begin
    if CNT_INT > 500 then
        PWM <= '0';
    else
        PWM <= '1';
    end if;
end process PWM1;

end Behavioral;
