----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TB_Counter is
end TB_Counter;

architecture Behavioral of TB_Counter is

COMPONENT Counter is
    Port ( CLK : in STD_LOGIC;
           RST : in STD_LOGIC;
           ENA : in STD_LOGIC;
           DOWN : in STD_LOGIC;
           LOAD_N : in STD_LOGIC;
           PWM : out STD_LOGIC;
           DIN : in STD_LOGIC_VECTOR (10 downto 0);
           CNT : out STD_LOGIC_VECTOR (10 downto 0));
end COMPONENT;

   signal CLK, RST, ENA, DOWN, LOAD_N, PWM  : STD_LOGIC := '0';
   signal DIN, CNT : STD_LOGIC_VECTOR (10 downto 0) := (others => '0');


begin

	-- Instantiate the Unit Under Test (UUT)
uut: Counter PORT MAP (
    CLK => CLK,
    RST => RST,
    ENA => ENA,
    DOWN => DOWN,
    LOAD_N => LOAD_N,
    PWM => PWM,
    DIN => DIN,
    CNT => CNT
);

-- Clock process definitions
CLK_process :process
begin
    CLK <= '0';
    wait for 10 ns;
    CLK <= '1';
    wait for 10 ns;
end process;

-- Stimulus process
stim_proc: process
begin        
  -- hold reset state for 100 ns.
    wait for 100 ns;
    RST <= '1';
    wait for 100 ns;
    RST <= '0';
    ENA <= '0';
    DOWN <= '0';
    LOAD_N <= '1';
    DIN <= "00000001010";
    wait for 100 ns;
    ENA <= '1';
    LOAD_N <= '0';
    wait for 100 ns;
    ENA <= '0';
    LOAD_N <= '1';
    wait for 100 ns;
    ENA <= '1';
    wait for 100 ns;
    ENA <= '0';
    DOWN <= '1';
    wait for 100 ns;
    ENA <= '1';
    wait for 100 ns;
    --ENA <= '0';
    wait;
end process;
end Behavioral;
