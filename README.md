# XC7 A15T Counter Project

This project consist of Counter Simulation made in Vivado.

## Getting Started

Start-up Vivado and open the Project from its cloned directory.

### Prerequisites

- [Vivado 2018.3](https://www.xilinx.com/support/download.html)

### Installing

Clone the repository into the desired directory and open it in Vivado

## Running the tests

Synthethize, Open the Syntethized design, Run Behavioral simulation for an appropiate time.

## Built With

* [Vivado Design Suite](https://www.xilinx.com/products/design-tools/vivado.html) -  Accelerating High Level Design

## Versioning

GitLab is used for versioning.

## Authors

* **Antonio Aguilar Mota** - *Initial work* - [AntoMota](https://gitlab.com/berkut3000)


## License

This project is licensed ...

## Acknowledgments

* [Dr. Jorge Alberto Soto Cajiga](https://cidesi.repositorioinstitucional.mx/jspui/browse?type=author&value=JORGE+ALBERTO+SOTO+CAJIGA)